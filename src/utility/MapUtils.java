package utility;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

/**
 * Classe utile ad effettuare varie operazioni sulle mappe.
 *
 * @param <K>
 * @param <V>
 * @param <T>
 */
public final class MapUtils<K, V, T> {

    private MapUtils() { }

    /**
     * Metodo per conteggiare il numero di elementi in una mappa.
     * Se per ogni chiave vi sono diversi elementi (es. delle List), contegger� gli elementi delle stesse.
     *
     * @param <K> 
     * @param <V> 
     * @param map - Map<K, V> da analizzare
     * @return int - numero di elementi
     */
    public static <K, V> int countObject(final Map<K, V> map) {
        return (int) map.entrySet().stream().mapToDouble(e -> e.getValue() instanceof Collection<?> ? ((Collection<?>) e.getValue()).stream().count() : 1).sum();
    }

    /**
     * Metodo che "collassa" una mappa su se stessa.
     * Elimina le chiavi che non hanno elementi o hanno elementi vuoti.
     * Restituisce la nuova mappa.
     * 
     * @param <K> 
     * @param <V> 
     * @param map - Map<K, V> da analizzare
     * @return nuova mappa modificata
     */
    public static <K, V> Map<K, V> collapse(final Map<K, V> map) {
        final Map<K, V> newMap = new HashMap<>(map);

        for (final K key : map.keySet()) {
            if (map.get(key).equals(Optional.empty()) || map.get(key) == null
                    || (map.get(key) instanceof Collection<?> && ((Collection<?>) map.get(key)).isEmpty())) {
                newMap.remove(key, map.get(key));
            }
        }
        return newMap;
    }

    /**
     * Metodo che restituisce la lista di chiavi che rispettano i seguenti requisiti: 
     * - hanno "value" come valore;
     * - hanno, come valori, delle collection che contengono value. 
     * Non analizza il caso in cui, come valori della mappa, siano presenti altre mappe.
     * 
     * @param <K> - tipo delle chiavi della mappa
     * @param <V> - tipo dei valori della mappa (può coincidere con <T>)
     * @param <T> - tipo del valore da cercare
     * 
     * @param map - Map<K, V> da analizzare
     * @param value - <T> valore da cercare
     * @return List<K>
     */
    @SuppressWarnings("unlikely-arg-type") //ho fatto il controllo per vedere se V == T
    public static <K, V, T> List<K> getKeyByValue(final Map<K, V> map, final T value) {
        final List<K> keys = new ArrayList<>();

        for (final Entry<K, V> entry : collapse(map).entrySet()) {
            if ((entry.getValue().getClass().equals(value.getClass()) && entry.getValue().equals(value)) || (entry.getValue() instanceof Collection<?> && ((Collection<?>) entry.getValue()).contains(value))) {
                keys.add(entry.getKey());
            }
        }
        return keys;
    }
}
