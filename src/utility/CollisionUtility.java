package utility;

import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import model.entities.Entity;

/**
 * Classe di utilita' per la gestione delle collisioni.
 */
public final class CollisionUtility {

    private CollisionUtility() { }

    /**
     * Metodo per controllare se due {@link Entity} collidono.
     * Inscrive ogni entita' in un rettangolo, poi sfrutta la classe {@link Rectangle} per controllare se collidono.
     *
     * @param first - prima entita'
     * @param second - seconda entita'
     *
     * @return true se i rettangoli creati collidono, false altrimenti.
     */
    public static boolean intersects(final Entity first, final Entity second) {
        final Shape s1 = new Rectangle(first.getMinX(), first.getMinY(), first.getMaxX() - first.getMinX(), first.getMaxY() - first.getMinY());
        final Shape s2 = new Rectangle(second.getMinX(), second.getMinY(), second.getMaxX() - second.getMinX(), second.getMaxY() - second.getMinY());

        return s1.intersects(s2.getBoundsInLocal());
    }

    /**
     * Verifica se la prima {@link Entity} collide con la parte orizzontale-inferiore della seconda entita'.
     * @param first - prima entita'
     * @param second - seconda entita'
     * @return true se collidono nella maniera sopra descritta, false altrimenti
     */
    public static boolean firstCollidedWithLowerHorizontalBound(final Entity first, final Entity second) {
        return first.getMaxX() >= second.getMinX() && first.getMinX() <= second.getMaxX() && first.getMaxY() >= second.getMaxY() ? true : false;
    }

    /**
     * Verifica se la prima {@link Entity} collide con la parte orizzontale-superiore della seconda entita'.
     * @param first - prima entita'
     * @param second - seconda entita'
     * @return true se collidono nella maniera sopra descritta, false altrimenti
     */
    public static boolean firstCollidedWithTopHorizontalBound(final Entity first, final Entity second) {
        return first.getMaxX() >= second.getMinX() && first.getMinX() <= second.getMaxX() && first.getMinY() <= second.getMinY() ? true : false;
    }

    /**
     * Verifica se la prima {@link Entity} collide con la parte verticale-destra della seconda entita'.
     * @param first - prima entita'
     * @param second - seconda entita'
     * @return true se collidono nella maniera sopra descritta, false altrimenti
     */
    public static boolean firstCollidedWithRightestVerticalBound(final Entity first, final Entity second) {
        return first.getMaxY() >= second.getMinY() && first.getMinY() <= second.getMaxY() && first.getMaxX() >= second.getMaxX() ? true : false;
    }

    /**
     * Verifica se la prima {@link Entity} collide con la parte verticale-sinistra della seconda entita'.
     * @param first - prima entita'
     * @param second - seconda entita'
     * @return true se collidono nella maniera sopra descritta, false altrimenti
     */
    public static boolean firstCollidedWithLeftestVerticalBound(final Entity first, final Entity second) {
        return first.getMaxY() >= second.getMinY() && first.getMinY() <= second.getMaxY() && first.getMinX() <= second.getMinX() ? true : false;
    }
}
