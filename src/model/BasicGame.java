package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import model.entities.Ball;
import model.entities.Bar;
import model.entities.Brick;
import model.entities.EntityFactory;
import model.entities.Entity;
import model.entities.IEntityFactory;
import model.entities.EntityThatMoves;
import model.entities.Wall;
import utility.CollisionUtility;
import utility.MapUtils;

/**
 * Classe che modella una partita "base".
 * Uniche entità in gioco: pallina, barra e mattoni.
 */
public class BasicGame implements IBasicGame {

    private static final int LIVES_ON_START = 3;
    private static final int SCORE_ON_START = 0;

    private GameStatus status;
    private int score;
    private int lives;

    private final List<Ball> balls;
    private final List<Wall> walls;
    private Bar bar;
    private Map<Integer, List<Brick>> bricks;

    private final IEntityFactory factory;

    /**
     * Costruttore che inizializza tutti i parametri di gioco.
     */
    public BasicGame() {
        this.status = GameStatus.START;
        this.score = SCORE_ON_START; 
        this.lives = LIVES_ON_START;

        this.factory = new EntityFactory();
        this.balls = new ArrayList<>();

        this.bricks = factory.randomBrickMap();
        this.walls = factory.standardWalls();
        this.bar = factory.standardBar();
    }

    @Override
    public final void initOnStart() {
        this.balls.add(factory.standardBall());
        this.bar = factory.standardBar();
        this.status = GameStatus.PAUSE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateModel() {
        final List<Entity> staticEntities = getAllEntities().stream().filter(ent -> !(ent instanceof EntityThatMoves)).collect(Collectors.toList());
        this.balls.forEach(e -> e.refreshPosition());

        for (final Ball ball : new ArrayList<>(this.balls)) {
            for (final Entity staticEnt : staticEntities) {
                if (CollisionUtility.intersects(ball, staticEnt)) {
                    ball.bounce(staticEnt);
                    if (staticEnt.getClass().equals(Brick.class)) {
                        this.removeEntity(staticEnt);
                    }
                }
            }
            outOfBound(ball);
            if (ball.getMaxY() > ModelCostant.WORLD_HEIGHT) {
                removeEntity(ball);
            }
        }
    }

    @Override
    public final void setPause() {
        if (this.status.equals(GameStatus.RUNNING)) {
            this.status = GameStatus.PAUSE;
        } else if (this.status.equals(GameStatus.PAUSE)) {
            this.status = GameStatus.RUNNING;
        }
    }

    @Override
    public final List<Ball> getBalls() {
        return this.balls;
    }


    @Override
    public final Map<Integer, List<Brick>> getBricks() {
        return this.bricks;
    }

    @Override
    public final Bar getBar() {
        return this.bar;
    }

    @Override
    public final int getScore() {
        return this.score;
    }

    @Override
    public final int getLives() {
        return this.lives;
    }


    @Override
    public final GameStatus getStatus() {
        return this.status;
    }

    /**
     * @return factory
     */
    protected IEntityFactory getFactory() {
        return this.factory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Entity> getAllEntities() {
        final List<Entity> list = new ArrayList<>();

        list.addAll(this.balls);
        list.addAll(this.walls);
        list.add(this.bar);

        for (final Entry<Integer, List<Brick>> entry : this.bricks.entrySet()) {
            list.addAll(entry.getValue());
        }

        return list;
    }

    /**
     * Metodo per decrementare il numero delle vite.
     */
    protected void lostLife() {
        this.lives--;

        if (this.lives == 0) {
            this.status = GameStatus.LOST;
        }
    }

    /**
     * @param b - {@link Ball} persa.
     */
    protected void lostBall(final Ball b) {
        this.balls.remove(b);
        if (this.balls.size() == 0) {
            this.status = GameStatus.START;
            lostLife();
        }
    }

    /**
     * Riporta la palla sul bordo se "entra" in una parete.
     * @param ball 
     */
    protected void outOfBound(final Ball ball) {
        int x = ball.getPosition().getKey();
        int y = ball.getPosition().getValue();

        x = x < 0 + ball.getRadius() ? ball.getRadius() : x > ModelCostant.WORLD_WIDTH - ball.getRadius() ? ModelCostant.WORLD_WIDTH - ball.getRadius() : x;
        y = y < 0 + ball.getRadius() ? ball.getRadius() : y;
        ball.setPosition(x, y);
    }

    /**
     * Metodo usato per "rompere" un mattone e rimuoverlo da quelli presenti in gioco.
     * 
     * @param row - numero della riga in cui si trova il mattone
     * @param brick - mattone da rimuovere
     */
    protected void breakBrick(final Integer row, final Brick brick) {
        this.getBricks().get(row).remove(brick);

        incScore(brick.getPoints());

        checkWin();
    }

    /**
     * Rimuove un'entità dal gioco.
     * 
     * @param entity - entità da rimuovere
     */
    protected void removeEntity(final Entity entity) {
        if (entity instanceof Ball) {
            lostBall((Ball) entity);
        }

        if (entity instanceof Brick) {
            final List<Integer> index = MapUtils.getKeyByValue(this.bricks, entity);
            if (!index.isEmpty()) {
                breakBrick(index.get(0), (Brick) entity);
            }
        }
    }

    /**
     * Modifica il punteggio, aggiungendo value a quello attuale.
     * @param value - punteggio da aggiungere
     */
    protected void incScore(final int value) {
        this.score += value;
    }

    /**
     * Modifica il numero delle vite.
     * @param newLives - nuovo numero delle vite.
     */
    protected void setLives(final int newLives) {
        this.lives = newLives;
    }

    private void checkWin() {
        if (MapUtils.countObject(this.getBricks()) == 0) {
            //|| bricks.entrySet().stream().filter(e -> e.getValue().stream().anyMatch(x -> x.isDistruttibile())).count() > 0) { //ci possono essere mattoni indistruttibili
            this.status = GameStatus.WIN;
        }
    }

    /**
     * @param newMap - nuova mappa di mattoni
     */
    protected final void setBricks(final Map<Integer, List<Brick>> newMap) {
        this.bricks = newMap;
    }
}
