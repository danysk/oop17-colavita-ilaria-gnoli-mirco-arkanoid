package model;

import model.entities.Brick;

/**
 * Classe concreta che modella il gioco in modalit� CLASSICA. Concretizza {@link AbstractGame}.
 * @author Gnoli Mirco
 *
 */
public class ClassicGame extends AbstractAdvancedGame {

    @Override
    protected final void doIfBricksRowIsEmpty(final int row, final Brick template) { 
    }
}
