package model.entities;

import java.util.Optional;

import javafx.geometry.HorizontalDirection;
import javafx.geometry.Side;
import javafx.geometry.VerticalDirection;
import javafx.util.Pair;

/**
 * Classe concreta che modella una pallina. Implementa {@link Entity}.
 */
public class Ball implements EntityThatMoves {

    private static final int ANGLE_ZERO = 0;
    private static final int ANGLE_RECT = 90;
    private static final int ANGLE_FLAT = 180;
    private static final int ANGLE_270 = 270;
    private static final int ANGLE_MAX = 360;

    private static final int COLLISION_ON_CHANGE = 20;
    private static final int CHANGE_ANGLE = 17;

    private Pair<Integer, Integer> position;
    private final int radius;
    private int speed;
    private int angle;
    private BallType type;
    private int nCollision;

    /**
     * Costruttore per creare una {@link Ball}.
     * 
     * @param x -Coordinata X del centro della pallina.
     * @param y -Coordinata y del centro della pallina.
     * @param radius - Raggio della pallina.
     * @param speed - Velocita' della pallina.
     * @param angle -Angolo di direzione. Angolo calcolato in sessagesimali, dall'asse delle ascisse in senso orario.
     * @param type - Tipologia di pallina, a scelta fra {@link BallType}
     * 
     * @see {@link BallBuilder}
     */
    //visibilità al solo package
    Ball(final int x, final int y, final int radius, final int speed, final int angle, final BallType type) {
        this.position = new Pair<>(x, y);
        this.radius = radius;
        this.speed = speed;
        this.angle = Math.abs(angle) % ANGLE_MAX;
        this.type = type;
        this.nCollision = 0;
    }

    @Override
    public final void refreshPosition() {
        final int newX = (int) (this.getPosition().getKey() + this.speed * Math.cos(Math.toRadians(angle)));
        final int newY = (int) (this.getPosition().getValue() + this.speed * Math.sin(Math.toRadians(angle)));
        this.setPosition(newX, newY);
    }

    /**
     * Incrementa la velocit� e modifica l'angolo della pallina ogni N collisioni.
     */
    private void increaseDifficult() {
        if (this.nCollision % COLLISION_ON_CHANGE == 0) {
            this.speed++;
            System.out.println("Aumentata la velocita' della pallina");

            this.angle += CHANGE_ANGLE;
            System.out.println("Modificato l'angolo di collisione");
            this.nCollision = 0;
        }
    }

    /**
     * Metodo che, preso in input la posizione della collisione, devia la pallina modificando l'angolo della stessa.
     * 
     * @param pos - {@link Side} di collisione della pallina.
     */
    public void doOnCollision(final Side pos) {
        switch (pos) {
        case TOP: case BOTTOM:
            this.angle = ANGLE_MAX - this.angle;
            break;

        case RIGHT:
            if (this.angle < ANGLE_RECT) {
                this.angle = ANGLE_FLAT - this.angle;
            } else {
                this.angle = (ANGLE_MAX - this.angle) + ANGLE_FLAT;
            }
            break;

        case LEFT:
            if (this.angle < ANGLE_FLAT) {
                this.angle = ANGLE_FLAT - this.angle;
            } else {
                this.angle = ANGLE_MAX - (this.angle - ANGLE_FLAT);
            }
            break;

        default:
            System.out.println("Non puo' entrare qui.");
        }
        /*Questi controlli evitano che la pallina rimbalzi orizzontale o verticale, mandando temporaneamente in stallo il gioco*/
        if ((this.angle >= ANGLE_ZERO && this.angle <= CHANGE_ANGLE) || (this.angle >= ANGLE_RECT && this.angle <= ANGLE_RECT + CHANGE_ANGLE)) {
            this.angle += CHANGE_ANGLE;
        } else if (this.angle >= ANGLE_MAX - CHANGE_ANGLE || (this.angle <= ANGLE_RECT && this.angle >= ANGLE_RECT - CHANGE_ANGLE)) {
            this.angle -= CHANGE_ANGLE;
        }

    }

    /**
     * Setta la nuova tipologia di pallina.
     * @param type - {@link BallType}
     */
    public final void setType(final BallType type) {
        this.type = type;
    }

    /**
     * {@inheritDoc}
     * I parametri in input settano il centro della pallina.
     */
    @Override
    public final void setPosition(final int newX, final int newY) {
        this.position = new Pair<>(newX, newY);
    }

    @Override
    public final int getMinX() {
        return this.position.getKey() - getRadius();
    }

    @Override
    public final int getMaxX() {
        return this.position.getKey() + getRadius();
    }

    @Override
    public final int getMinY() {
        return this.position.getValue() - getRadius();
    }

    @Override
    public final int getMaxY() {
        return this.position.getValue() + getRadius();
    }

    @Override
    public final Pair<Integer, Integer> getPosition() {
        return position;
    }

    /**
     * Ritorna il raggio della pallina.
     *
     * @return int
     */
    public final int getRadius() {
        return this.radius;
    }

    /**
     * Ritorna la velocita' della pallina. 
     * 
     * @return int
     */
    public final int getSpeed() {
        return speed;
    }

    /**
     * Ritorna l'angolo di direzione della pallina. 
     * 
     * @return int
     */
    public final int getAngle() {
        return angle;
    }

    /**
     * Ritorna la tipologia della pallina. 
     * 
     * @return {@link BallType}
     */
    public final BallType getType() {
        return this.type;
    }

    /**
     * Metodo per ritornare la direzione.
     * 
     * @return Pair(Optional(HorizontalDirection), Optional(VerticalDirection))
     */
    public Pair<Optional<HorizontalDirection>, Optional<VerticalDirection>> getDirection() {
        Optional<HorizontalDirection> oriz = Optional.empty();
        Optional<VerticalDirection> vert = Optional.empty();

        if (this.angle > 0 && this.angle < ANGLE_FLAT) {
            vert = Optional.of(VerticalDirection.DOWN);
        } else if (this.angle > ANGLE_FLAT && this.angle < ANGLE_MAX) {
            vert = Optional.of(VerticalDirection.UP);
        }

        if (this.angle > ANGLE_270 && this.angle <= ANGLE_MAX || this.angle >= ANGLE_ZERO && this.angle < ANGLE_RECT) {
            oriz = Optional.of(HorizontalDirection.RIGHT);
        } else if (this.angle > 90 && this.angle < ANGLE_270) {
            oriz = Optional.of(HorizontalDirection.LEFT);
        }

        return new Pair<>(oriz, vert);
    }

    /**
     * @param entity - {@link Entity} contro cui rimbalza.
     */
    public void bounce(final Entity entity) {
        this.nCollision++;
        this.type.bounce(this, entity);
        increaseDifficult();
    }
}

