package model.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Random;

import javafx.util.Pair;
import model.ModelCostant;
/**
 * Classe concreta factory delle varie {@link Entity}. Implementa {@link IEntityFactory} 
 *
 * @author Gnoli Mirco
 */
public class EntityFactory implements IEntityFactory {
    /**
     * Dimensione del raggio della pallina.
     */
    private static final int DEFAULT_RADIUS = 7;
    /**
     * Velocit� iniziale della pallina. 
     */
    private static final int DEFAULT_SPEED = 7;
    /**
     * Angolo iniziale della pallina. 
     */
    private static final int DEFAULT_ANGLE = 300;
    /**
     * Larghezza dei mattoni.
     */
    private static final int DEFAULT_BRICK_WIDTH = (ModelCostant.WORLD_WIDTH - ModelCostant.NUMBER_OF_COLUMN) / ModelCostant.NUMBER_OF_COLUMN;
    /**
     * Altezza dei mattoni.
     */
    //private static final int DEFAULT_BRICK_HEIGHT = Math.min(((ModelCostant.WORLD_HEIGHT / 3) / ModelCostant.NUMBER_OF_ROW), (DEFAULT_BRICK_WIDTH / 2));
    private static final int DEFAULT_BRICK_HEIGHT = 22;

    /**
     * Larghezza della barra.
     */
    public static final int DEFAULT_BAR_WIDTH = ModelCostant.WORLD_WIDTH / 6;

    /**
     * Altezza della barra.
     */
    public static final int DEFAULT_BAR_HEIGHT = 15;

    /**
     * Larghezza dei power-up.
     */
    private static final int POWERUP_WIDTH = Math.min(DEFAULT_BRICK_WIDTH, (ModelCostant.WORLD_WIDTH / 10));
    /**
     * Altezza dei power-up.
     */
    private static final int POWERUP_HEIGHT = DEFAULT_BRICK_HEIGHT;

    /**
     * Velocit� di caduta dei potenziamenti.
     */
    private static final int DEFAULT_POWERUP_SPEED = 4;
    /**
     * Probabilit� che un mattone rotto generi un power_up.
     */
    private static final double POWERUP_SPAWN_PROBABILITY = 0.35;

    private static final int MAX_DELTA_ANGLE = 25;
    private static final int MIN_DELTA_ANGLE = 5;

    @Override
    public final Ball standardBall() {
        return new BallBuilder()
                .position(ModelCostant.WORLD_WIDTH / 2, ModelCostant.WORLD_HEIGHT / 2)
                .radius(DEFAULT_RADIUS)
                .angle(DEFAULT_ANGLE)
                .speed(DEFAULT_SPEED)
                .type(BallType.STANDARD_BALL)
                .build();
    }

    @Override
    public final List<Ball> multipleBall(final Ball b) {
        final List<Ball> balls = new ArrayList<>();
        final int angle = new Random().nextInt(MAX_DELTA_ANGLE) + MIN_DELTA_ANGLE;
        balls.add(new BallBuilder()
                .position(b.getPosition().getKey(), b.getPosition().getValue())
                .radius(b.getRadius())
                .speed(b.getSpeed())
                .angle(b.getAngle() + angle)
                .type(b.getType())
                .build());

        balls.add(new BallBuilder()
                .position(b.getPosition().getKey(), b.getPosition().getValue())
                .radius(b.getRadius())
                .speed(b.getSpeed())
                .angle(b.getAngle() - angle)
                .type(b.getType())
                .build());
        return balls;
    }

    @Override
    public final List<Wall> standardWalls() {
        final List<Wall> walls = new ArrayList<>();
        walls.add(new Wall(0, 0, 0, ModelCostant.WORLD_HEIGHT));
        walls.add(new Wall(0, 0, ModelCostant.WORLD_WIDTH, 0));
        walls.add(new Wall(ModelCostant.WORLD_WIDTH, 0, ModelCostant.WORLD_WIDTH, ModelCostant.WORLD_HEIGHT));

        //parete inferiore, utile per i test
        //walls.add(new Wall(0, ModelCostant.WORLD_HEIGHT, ModelCostant.WORLD_WIDTH, ModelCostant.WORLD_HEIGHT));
        return walls;
    }

    private Brick randomBrick() {
        final Brick b = new Brick(DEFAULT_BRICK_WIDTH, DEFAULT_BRICK_HEIGHT);
        b.setBrickType(BrickType.values()[new Random().nextInt(BrickType.values().length)]);

        return b;
    }

    @Override
    public final List<Brick> randomBrickRow() {
        final List<Brick> rowBrick = new ArrayList<>();
        for (int i = 0; i < ModelCostant.NUMBER_OF_COLUMN; i++) {
            final Brick b = randomBrick();
            b.setPosition((int) (i + i * b.getWidth()), 0);
            rowBrick.add(b);
        }
        return rowBrick;
    }

    private List<Brick> brickRowWithType(final BrickType type) {
        final List<Brick> rowBrick = new ArrayList<>();
        for (int i = 0; i < ModelCostant.NUMBER_OF_COLUMN; i++) {
            final Brick b = randomBrick();
            b.setBrickType(type);
            b.setPosition((int) (i + i * b.getWidth()), 0);
            rowBrick.add(b);
        }
        return rowBrick;
    }
    //METODO CHE DISEGNA IN MANIERA RANDOM I MATTONI
    /*
    @Override
    public final Map<Integer, List<Brick>> randomBrickMap() {
        Map<Integer, List<Brick>> mapBrick = new HashMap<>();
        for (int i = 0; i < ModelCostant.NUMBER_OF_ROW; i++) {
            List<Brick> list = randomBrickRow();
            for (Brick b : list) {
                b.setPosition(b.getPosition().getKey(), (int) (i + i * b.getHeight()));
            }
            mapBrick.put(i, list);
        }
        return mapBrick;
    }
    */
    //METODO CHE DISEGNA IN FILA I MATTONI
    @Override
    public final Map<Integer, List<Brick>> randomBrickMap() {
        final Map<Integer, List<Brick>> mapBrick = new HashMap<>();

        mapBrick.put(0, brickRowWithType(BrickType.GREY));
        mapBrick.put(1, brickRowWithType(BrickType.RED));
        mapBrick.put(2, brickRowWithType(BrickType.YELLOW));
        mapBrick.put(3, brickRowWithType(BrickType.BLUE));
        mapBrick.put(4, brickRowWithType(BrickType.PINK));
        for (final Entry<Integer, List<Brick>> entry : mapBrick.entrySet()) {
            for (final Brick b : entry.getValue()) {
                b.setPosition(b.getPosition().getKey(), (int) (entry.getKey() + entry.getKey() * b.getHeight()));
            }
        }

        return mapBrick;
    }

    @Override
    public final Bar standardBar() {
        final int barXPos = (ModelCostant.WORLD_WIDTH - DEFAULT_BAR_WIDTH) / 2;
        final int barYPos = ModelCostant.WORLD_HEIGHT - DEFAULT_BAR_HEIGHT - ModelCostant.DEFAULT_OFFSET_FROM_WALL;
        return new Bar(barXPos, barYPos, DEFAULT_BAR_WIDTH, DEFAULT_BAR_HEIGHT, ModelCostant.DEFAULT_ARC, ModelCostant.DEFAULT_ARC);
    }

    @Override
    public final Optional<PowerUp> randomPowerUp(final Brick brickGenerator) {
        if (toBeGenerate()) {
            final Pair<Integer, Integer> pos = new Pair<>(brickGenerator.getPosition().getKey() + (int) ((DEFAULT_BRICK_WIDTH - POWERUP_WIDTH) / 2), brickGenerator.getPosition().getValue());
            return Optional.of(new PowerUp(POWERUP_WIDTH, POWERUP_HEIGHT, pos, DEFAULT_POWERUP_SPEED, (PowerUpType.values()[new Random().nextInt(PowerUpType.values().length)])));
        }
        return Optional.empty();
    }

    private boolean toBeGenerate() {
        return new Random().nextInt(100) < (POWERUP_SPAWN_PROBABILITY * 100);
    }

    @Override
    public final List<Projectile> pairOfProjectile(final Bar bar) {
        final List<Projectile> projectiles = new ArrayList<>();
        projectiles.add(new Projectile(new Pair<>(bar.getMinX(), bar.getMinY())));
        projectiles.add(new Projectile(new Pair<>(bar.getMaxX(), bar.getMinY())));

        return projectiles;
    }
}
