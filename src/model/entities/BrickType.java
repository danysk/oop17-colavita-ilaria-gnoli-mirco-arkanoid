package model.entities;

import javafx.scene.paint.Color;

/**
 * Enumeration which defines the different types of bricks that the application can display. 
 * Every type of brick has a color and a counter (It indicate how many times the brick needs to be hit until it get destroyed)
 */
public enum BrickType {

    /**
     * grey - needs 1 hit - add 100 points.
     */
    GREY(Color.DARKGREY, 1, 100),

    /**
     * red - needs 2 hits - add 200 points.
     */
    RED(Color.INDIANRED, 2, 200),

    /**
     * yellow - needs 1 hit - add 100 points.
     */
    YELLOW(Color.YELLOW, 1, 100),

    /**
     * blue - needs 2 hits - add 200 points.
     */
    BLUE(Color.BLUE, 2, 200),

    /**
     * pink - need 1 hit - add 50 points.
     */
    PINK(Color.PINK, 1, 50);

    private final Color color;
    private int counter;
    private int points;

    BrickType(final Color color, final int counter, final int points) {
        this.color = color;
        this.counter = counter;
        this.points = points;
    }
    /**
     * Get the brick color.
     * 
     * @return the brick color
     */
    public Color getColor() {
        return this.color;
    }

    /**
     * Get the brick counter.
     * 
     * @return the brick counter
     */
    public int getCounter() {
        return this.counter;
    }

    /**
     * Get the points to add at brick break.
     * 
     * @return the points
     */
    public int getPoints() {
        return this.points;
    }
}
