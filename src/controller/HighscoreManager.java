package controller;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import javafx.util.Pair;
import model.GameMode;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Classe singleton per gestire i punteggi.
 */
public final class HighscoreManager {
    private static HighscoreManager instance;

    private static final String PATH = System.getProperty("user.home") + System.getProperty("file.separator");
    private static final String MODE_CLASSIC = "Classic";
    private static final String MODE_STANDARD = "Standard";
    private static final String MODE_SURVIVAL = "Survival";

    private final List<Pair<String, Integer>> classicScore;
    private final List<Pair<String, Integer>> standardScore;
    private final List<Pair<String, Integer>> survivalScore;

    private HighscoreManager() {
        this.classicScore = loadScore(MODE_CLASSIC);
        this.standardScore = loadScore(MODE_STANDARD);
        this.survivalScore = loadScore(MODE_SURVIVAL);
    }

    /**
     * @return l'istanza di questa classe.
     */
    public static HighscoreManager getInstance() {
        if (Objects.isNull(instance)) {
            instance = new HighscoreManager();
        }
        return instance;
    }


    /**
     * Aggiunge un punteggio.
     * @param name - nome del giocatore.
     * @param score - punteggio 
     * @param mode - modalit� di gioco scelta fra quelle disponibili in {@link GameMode}
     */
    public void addScore(final String name, final Integer score, final GameMode mode) {

        switch (mode) {
        case CLASSIC:
            this.classicScore.add(new Pair<>(name, score));
            Collections.sort(this.classicScore, new MyCompare());
            break;

        case STANDARD:
            this.standardScore.add(new Pair<>(name, score));
            Collections.sort(this.standardScore, new MyCompare());
            break;

        case SURVIVAL:
            this.survivalScore.add(new Pair<>(name, score));
            Collections.sort(this.survivalScore, new MyCompare());
            break;

        default:
            System.out.println("errore creare eccezione");
            break;
        }
    }


    /**
     * Metodo per caricare tutti i punteggi dal file.
     * 
     * @param modalit� di gioco
     * @return lista di {@link Pair} (String, Integer)
     */
    @SuppressWarnings("unchecked")
    private List<Pair<String, Integer>> loadScore(final String mode) {
        List<Pair<String, Integer>> load = new ArrayList<>();

        final File f = new File(PATH + mode);
        try {
            f.createNewFile();

            if (f.length() > 0) {
                final ObjectInputStream ostream = new ObjectInputStream(new BufferedInputStream(new FileInputStream(PATH + mode)));

                load = (List<Pair<String, Integer>>) ostream.readObject();
                ostream.close();
            }

        } catch (IOException e) {
            System.out.println("Errore file " + f + " corrotto. Eliminare il file e riavviare l'applicazione");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return load;
    }

    /**
     * Metodo per salvare tutti i punteggi su file.
     */
    public void saveAllScores() {
        saveScore(MODE_CLASSIC, classicScore);
        saveScore(MODE_STANDARD, standardScore);
        saveScore(MODE_SURVIVAL, survivalScore);
    }

    /**
     * Metodo per salvare i punteggi di una determinata modalit�.
     * 
     * @param mode
     * @param lista di punteggi per quella modalit�.
     */
    private void saveScore(final String mode, final List<Pair<String, Integer>> list) {
        try {
            final ObjectOutputStream ostream = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(PATH + mode)));

            ostream.writeObject(list);
            ostream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo per ottenere la lista dei punteggi per una determinata modalit�.
     * 
     * @param mode - modalit� a scelta fra {@link GameMode}
     * @return lista di Pair(String, Integer)
     */
    public List<Pair<String, Integer>> getScore(final GameMode mode) {
        switch (mode) {
        case CLASSIC:
            Collections.sort(this.classicScore, new MyCompare());
            return this.classicScore;

        case STANDARD:
            Collections.sort(this.standardScore, new MyCompare());
            return this.standardScore;

        case SURVIVAL:
            Collections.sort(this.survivalScore, new MyCompare());
            return this.survivalScore;

        default:
            System.out.println("Errore durante l'acquisizione dei punteggi.");
            return null;
        }
    }

    /**
     * Semplice comparatore per ordinare in ordine crescente le coppie (nome, punteggio) in base al punteggio.
     */
    private class MyCompare implements Comparator<Pair<String, Integer>> {

        @Override
        public final int compare(final Pair<String, Integer> o1, final Pair<String, Integer> o2) {
            return Integer.compare(o2.getValue(), o1.getValue());
        }
    }
}
