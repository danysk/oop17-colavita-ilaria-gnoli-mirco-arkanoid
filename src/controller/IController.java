package controller;

import model.IBasicGame;
import view.GameView;
/**
 * Generica interfaccia che modella un controllore.
 */
public interface IController {
    /**
     * Salva il punteggio ottenuto.
     * @param name - nome del giocatore
     */
    void saveScore(String name);

    /**
     * Metodo per aprire il menu principale.
     */
    void openMenu();

    /**
     * @return view responsabile della visualizzazione del gioco.
     */
    GameView getView();

    /**
     * @return modello del gioco.
     */
    IBasicGame getGame();
}
