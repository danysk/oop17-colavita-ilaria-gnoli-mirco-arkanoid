package controller;

import java.util.List;

import model.entities.Entity;
/**
 * Modellazione di un soggetto da notificare per aggiornare la visualizzazione.
 */
public interface IObserver {
    /**
     * Aggiornamento di punteggio e vite.
     * @param score 
     * @param lives 
     */
    void updateScoreLives(int score, int lives);

    /**
     * Aggiornamento di tutte le entita' rappresentate.
     * @param entities - entita' da disegnare
     */
    void updateAllEntities(List<Entity> entities);
}
