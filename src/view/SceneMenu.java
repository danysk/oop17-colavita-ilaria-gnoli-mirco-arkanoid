package view;

import javafx.scene.Node;
/**
 * Interfaccia che definisce il contratto di qualunque scena del menu' principale (non di gioco).
 * Tutte le scene si sviluppano su un {@link BorderPane}
 */
public interface SceneMenu {

    /**
     * @return Pannello centrale della scena.
     */
    Node centerPane();

    /**
     * @return Pannello superiore della scena, solitamente racchiude il titolo.
     */
    Node topPane();

    /** 
     * @return Pannello inferiore della scena.
     */
    Node bottomPane();

    /**
     * @return String - titolo della scena rappresentata.
     */
    String getTitle();
}
