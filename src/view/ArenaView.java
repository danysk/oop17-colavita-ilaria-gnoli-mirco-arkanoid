package view;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Shape;
import model.ModelCostant;
import model.entities.Entity;
import utility.Mapper;
import view.utils.ImageViewObject;

/**
 * Pannello che rappresenta il campo da gioco. Estende {@link Pane}
 * Tutti gli oggetti del gioco vengono disegnati qui sopra.
 */
public class ArenaView extends Pane {

    /**
     *
     */
    public ArenaView() {
        this.setPrefWidth(ModelCostant.WORLD_WIDTH);
        this.setPrefHeight(ModelCostant.WORLD_HEIGHT);
        final BackgroundImage bgimg = new BackgroundImage(ImageViewObject.SFONDO_ARENA.getImage(),  BackgroundRepeat.NO_REPEAT, BackgroundRepeat.ROUND, BackgroundPosition.CENTER,  new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, true));
        this.setBackground(new Background(bgimg));
    }

    private List<? extends Node> transform(final List<Entity> ent) {
        final List<Optional<Shape>> list = new ArrayList<>();
        for (final Entity e : ent) {
            list.add(Mapper.entityToView(e));
        }
        return list.stream().filter(e -> e.isPresent()).map(e -> e.get()).collect(Collectors.toList());
    }

    /**
     * @param entities - Lista di entity da rappresentare su schermo.
     */
    public final void refresh(final List<Entity> entities) {    //rimuove tutto e rimette tutto, da migliorare
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                getChildren().removeAll(getChildren());
                getChildren().addAll(transform(entities));
            }
        });
    }
}
